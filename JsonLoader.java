package rs.etf.navigation.db;

import android.content.Context;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import rs.etf.navigation.location.Location;
import rs.etf.navigation.place.Beacon;
import rs.etf.navigation.place.ConstructionElement;
import rs.etf.navigation.place.Exhibition;
import rs.etf.navigation.place.FlorMap;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.Museum;
import rs.etf.navigation.place.MuseumMap;
import rs.etf.navigation.place.Picture;
import rs.etf.navigation.place.Room;

public class JsonLoader {
    //public static final String URL_ = "http://192.168.0.16:3000/master-0.0.1-SNAPSHOT/";
    public static final String URL_ = "http://185.103.219.198:8081/master-0.0.1-SNAPSHOT/";
    //public static final String URL_ = "http://172.20.10.13:3000/master-0.0.1-SNAPSHOT/";
    private static List<Picture> sPicturesList;

    public static Museum createMuseum(Context context) {
        int id = 85;
        String myUrl = URL_ + "provider/museums/" + id;
        JSONObject result = null;
        Museum museum = null;
        HttpResponse response = null;
        HttpGetRequestObject getRequest = new HttpGetRequestObject(context);
        try {
            result = getRequest.execute(myUrl).get();
            museum = new Museum();
            museum.setName(result.getString("name"));
            museum.setInstitution(result.getString("institution"));
            museum.setLanguage(result.getString("language"));
            museum.setText(result.getString("text"));
            museum.setTextUrl(result.getString("textUrl"));
            //check if it exists
            if (!result.get("xCoordinate").equals(null) || !result.get("yCoordinate").equals(null) || !result.get("zCoordinate").equals(null)) {
                museum.setLocation(new Location(result.getDouble("xCoordinate"), result.getDouble("yCoordinate"), result.getDouble("zCoordinate")));
            }
            Integer idMuseum = result.getInt("id");
            List<Item> items = getItems(context, idMuseum);
            List<FlorMap> floormaps = getFloormaps(context, idMuseum);
            List<Beacon> beacons = getBeacons(context, idMuseum);
            List<MuseumMap> museummaps = getMuseummapsAndConnect(context, idMuseum, floormaps, items, beacons);
            List<Exhibition> exhibitions = getExhibitionsAndConnect(context, idMuseum, items, museummaps);
            List<Room> rooms = getRoomsAndConnect(context, idMuseum, floormaps, items);
            museum.setItems(items);
            museum.setExhibitions(exhibitions);
            museum.setMaps(floormaps);
            museum.setMuseumMaps(museummaps);
            museum.setRooms(rooms);
            museum.setBeacons(beacons);

            museum.setPrimaryImage(result.getString("primaryClientImageUrl"));
            museum.setSecondaryImage(result.getString("secondaryClientImageUrl"));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return museum;
    }

    public static List<Item> getItems(Context context, Integer idMuseum) {
        String myUrl = URL_ + "provider/items/" + idMuseum;
        JSONArray resultList = null;
        List<Item> items = new ArrayList<>();
        HttpGetRequestArray getRequest = new HttpGetRequestArray(context);
        try {
            resultList = getRequest.execute(myUrl).get();
            for (int i = 0; i < resultList.length(); i++) {
                JSONObject result = resultList.getJSONObject(i);
                Item item = new Item();
                item.setId(result.getString("id"));
                item.setName(result.getString("name"));
                item.setInstitution(result.getString("institution"));
                item.setLanguage(result.getString("language"));
                item.setAuthor(result.getString("author"));
                item.setQr(result.getString("barcodeUrl"));
                if (!result.get("radius").equals(null)) {
                    item.setRadius(result.getDouble("radius"));
                }
                if (!result.get("xCoordinate").equals(null) && !result.get("yCoordinate").equals(null) && !result.get("zCoordinate").equals(null)) {
                    item.setLocation(new Location(result.getDouble("xCoordinate"), result.getDouble("yCoordinate"), result.getDouble("zCoordinate")));
                }
                //todo: set texts, images, audios, videos
                JSONArray imageUrls = (JSONArray) result.get("imageUrls");
                JSONArray audioUrls = (JSONArray) result.get("audioUrls");
                JSONArray videoUrls = (JSONArray) result.get("videoUrls");
                JSONArray texts = (JSONArray) result.get("texts");
                if (imageUrls.length() != 0) {
                    item.setImageUrl(imageUrls.get(0).toString());
                }
                if (audioUrls.length() != 0) {
                    item.setAudioUrl(audioUrls.get(0).toString());
                }
                if (videoUrls.length() != 0) {
                    item.setVideoUrl(videoUrls.get(0).toString());
                }
                if (texts.length() != 0) {
                    item.setText(texts.get(0).toString());
                }
                items.add(item);
            }

        } catch (ExecutionException | JSONException | InterruptedException e) {
            e.printStackTrace();
        }
        return items;
    }

    public static List<Beacon> getBeacons(Context context, Integer idMuseum) {
        String myUrl = URL_ + "provider/beacons/" + idMuseum;
        JSONArray resultList = null;
        List<Beacon> beacons = new ArrayList<>();
        HttpGetRequestArray getRequest = new HttpGetRequestArray(context);
        try {
            resultList = getRequest.execute(myUrl).get();
            for (int i = 0; i < resultList.length(); i++) {
                JSONObject result = resultList.getJSONObject(i);
                Beacon beacon = new Beacon();
                beacon.setId(result.getString("id"));
                beacon.setUuid(UUID.randomUUID());
                beacon.setName(result.getString("name"));
                beacon.setInstitution(result.getString("institution"));
                if (!result.get("minor").equals(null)) {
                    beacon.setMinor(result.getInt("minor"));
                }
                if (!result.get("major").equals(null)) {
                    beacon.setMajor(result.getInt("major"));
                }
                if (!result.get("power").equals(null)) {
                    beacon.setPower(result.getInt("power"));
                }
                if (!result.get("radius").equals(null)) {
                    beacon.setRadius(result.getDouble("radius"));
                }
                if (!result.get("xcoordinate").equals(null) && !result.get("ycoordinate").equals(null) && !result.get("zcoordinate").equals(null)) {
                    beacon.setLocation(new Location(result.getDouble("xcoordinate"), result.getDouble("ycoordinate"), result.getDouble("zcoordinate")));
                }
                beacons.add(beacon);
            }

        } catch (ExecutionException | JSONException | InterruptedException e) {
            e.printStackTrace();
        }
        return beacons;
    }

    public static List<FlorMap> getFloormaps(Context context, Integer idMuseum) {
        String myUrl = URL_ + "provider/floormaps/" + idMuseum;
        JSONArray resultList = null;
        List<FlorMap> floormaps = new ArrayList<>();
        HttpGetRequestArray getRequest = new HttpGetRequestArray(context);
        try {
            resultList = getRequest.execute(myUrl).get();
            for (int i = 0; i < resultList.length(); i++) {
                JSONObject result = resultList.getJSONObject(i);
                FlorMap floormap = new FlorMap();
                floormap.setId(result.getString("id"));
                floormap.setName(result.getString("name"));
                floormap.setInstitution(result.getString("institution"));
                floormap.setLanguage(result.getString("language"));
                floormap.setText(result.getString("text"));
                floormap.setFlorNumber(result.getInt("floorNumber"));
                if (!result.get("xStart").equals(null) && !result.get("yStart").equals(null) && !result.get("zCoordinate").equals(null)) {
                    floormap.setStart(new Location(result.getDouble("xStart"), result.getDouble("yStart"), result.getDouble("zCoordinate")));
                }
                if (!result.get("xEnd").equals(null) && !result.get("yEnd").equals(null) && !result.get("zCoordinate").equals(null)) {
                    floormap.setEnd(new Location(result.getDouble("xEnd"), result.getDouble("yEnd"), result.getDouble("zCoordinate")));
                }
                floormap.setImageUrl(result.getString("imageUrl"));
                floormap.setAudioUrl(result.getString("audioUrl"));
                floormap.setVideoUrl(result.getString("videoUrl"));
                floormaps.add(floormap);
            }

        } catch (ExecutionException | JSONException | InterruptedException e) {
            e.printStackTrace();
        }
        return floormaps;
    }

    public static List<MuseumMap> getMuseummapsAndConnect(Context context, Integer idMuseum, List<FlorMap> floorMaps, List<Item> items, List<Beacon> beacons) {
        String myUrl = URL_ + "provider/museummaps/" + idMuseum;
        JSONArray resultList = null;
        List<MuseumMap> museummaps = new ArrayList<>();
        HttpGetRequestArray getRequest = new HttpGetRequestArray(context);
        try {
            resultList = getRequest.execute(myUrl).get();
            for (int i = 0; i < resultList.length(); i++) {
                JSONObject result = resultList.getJSONObject(i);
                MuseumMap museumMap = new MuseumMap();
                museumMap.setId(result.getString("id"));
                museumMap.setName(result.getString("name"));
                museumMap.setInstitution(result.getString("institution"));
                museumMap.setLanguage(result.getString("language"));
                museumMap.setText(result.getString("text"));
                museumMap.setTextUrl(result.getString("textUrl"));
                if (!result.get("xStart").equals(null) && !result.get("yStart").equals(null) && !result.get("zCoordinate").equals(null)) {
                    museumMap.setLocation(new Location(result.getDouble("xStart"), result.getDouble("yStart"), result.getDouble("zCoordinate")));
                }
                //set images, audios, videos
                JSONArray imageUrls = (JSONArray) result.get("imageUrls");
                JSONArray audioUrls = (JSONArray) result.get("audioUrls");
                JSONArray videoUrls = (JSONArray) result.get("videoUrls");
                if (imageUrls.length() != 0) {
                    museumMap.setImageUrl(imageUrls.get(0).toString());
                }
                if (audioUrls.length() != 0) {
                    museumMap.setAudioUrl(audioUrls.get(0).toString());
                }
                if (videoUrls.length() != 0) {
                    museumMap.setVideoUrl(videoUrls.get(0).toString());
                }

                if (floorMaps != null) {
                    for (FlorMap f : floorMaps) {
                        if (Integer.valueOf(f.getId()) == result.getInt("floormapId")) {
                            museumMap.setFlorMap(f);
                            break;
                        }
                    }
                }
                List<Item> itemsList = new ArrayList<>();
                JSONArray itemIds = (JSONArray) result.get("itemsIds");
                if (itemIds.length() != 0 && !items.isEmpty()) {
                    for (Item p : items) {
                        if (itemIds.toString().contains(p.getId())) {
                            itemsList.add(p);
                        }
                    }
                }
                museumMap.setItems(itemsList);
                List<Beacon> beaconList = new ArrayList<>();
                JSONArray beaconIds = (JSONArray) result.get("beaconIds");
                if (beaconIds.length() != 0 && !beacons.isEmpty()) {
                    for (Beacon b : beacons) {
                        if (beaconIds.toString().contains(b.getId())) {
                            beaconList.add(b);
                        }
                    }
                }
                museumMap.setBeacons(beaconList);
                museummaps.add(museumMap);
            }
        } catch (ExecutionException | JSONException | InterruptedException e) {
            e.printStackTrace();
        }
        return museummaps;
    }

    public static List<Room> getRoomsAndConnect(Context context, Integer idMuseum, List<FlorMap> floorMaps, List<Item> items) {
        String myUrl = URL_ + "provider/rooms/" + idMuseum;
        JSONArray resultList = null;
        List<Room> rooms = new ArrayList<>();
        HttpGetRequestArray getRequest = new HttpGetRequestArray(context);
        try {
            resultList = getRequest.execute(myUrl).get();
            for (int i = 0; i < resultList.length(); i++) {
                JSONObject result = resultList.getJSONObject(i);
                Room room = new Room();
                room.setId(result.getString("id"));
                room.setName(result.getString("name"));
                room.setInstitution(result.getString("institution"));
                room.setLanguage(result.getString("language"));
                room.setText(result.getString("text"));
                room.setTextUrl(result.getString("textUrl"));
                android.graphics.Point start = null;
                android.graphics.Point end = null;
                if (!result.get("xStart").equals(null) && !result.get("yStart").equals(null) && !result.get("zCoordinate").equals(null)) {
                    start = new android.graphics.Point((int) result.getDouble("xStart"), (int) result.getDouble("yStart"));
                }
                if (!result.get("xEnd").equals(null) && !result.get("yEnd").equals(null) && !result.get("zCoordinate").equals(null)) {
                    end = new android.graphics.Point((int) result.getDouble("xEnd"), (int) result.getDouble("yEnd"));
                }
                ConstructionElement element = new ConstructionElement(start, end, 1);
                List<ConstructionElement> elements = new ArrayList();
                elements.add(element);
                room.setConstructionElements(elements);
                if (!result.get("xStart").equals(null) && !result.get("yStart").equals(null) && !result.get("zCoordinate").equals(null)) {
                    room.setLocation(new Location(result.getDouble("xStart"), result.getDouble("yStart"), result.getDouble("zCoordinate")));
                }
                if (floorMaps != null) {
                    for (FlorMap f : floorMaps) {
                        if (Integer.valueOf(f.getId()) == result.getInt("floormapId")) {
                            room.setMap(f);
                            break;
                        }
                    }
                }
                List<Item> itemsList = new ArrayList<>();
                JSONArray itemIds = (JSONArray) result.get("itemsIds");
                if (itemIds.length() != 0 && !items.isEmpty()) {
                    for (Item p : items) {
                        if (itemIds.toString().contains(p.getId())) {
                            itemsList.add(p);
                        }
                    }
                }
                room.setItems(itemsList);

                List<Room> roomList = new ArrayList<>();
                JSONArray roomIds = (JSONArray) result.get("roomsIds");
                if (roomIds.length() != 0 && !rooms.isEmpty()) {
                    for (Room r : rooms) {
                        if (roomIds.toString().contains(r.getId())) {
                            roomList.add(r);
                        }
                    }
                }
                room.setConnected(roomList);
                rooms.add(room);
            }

        } catch (ExecutionException | JSONException | InterruptedException e) {
            e.printStackTrace();
        }
        return rooms;
    }

    public static List<Exhibition> getExhibitionsAndConnect(Context context, Integer idMuseum, List<Item> items, List<MuseumMap> museumMaps) {
        String myUrl = URL_ + "provider/exhibitions/" + idMuseum;
        JSONArray resultList = null;
        List<Exhibition> exhibitions = new ArrayList<>();
        HttpGetRequestArray getRequest = new HttpGetRequestArray(context);
        try {
            resultList = getRequest.execute(myUrl).get();
            if (resultList != null) {
                for (int i = 0; i < resultList.length(); i++) {
                    JSONObject result = resultList.getJSONObject(i);
                    Exhibition exhibition = new Exhibition();
                    exhibition.setId(result.getString("id"));
                    exhibition.setName(result.getString("name"));
                    exhibition.setAuthor(result.getString("author"));
                    exhibition.setInstitution(result.getString("institution"));
                    exhibition.setLanguage(result.getString("language"));
                    //todo: this is connected with pictures, what with that? every image that is set to exhibition, should have different image, audio and video url that is connected to that exhibition directly
                    //List<String> texts = (List<String>) result.get("texts");
                    //List<String> imageUrls = (List<String>) result.get("imageUrls");
                    //List<String> audioUrls = (List<String>) result.get("audioUrls");
                    //List<String> videoUrls = (List<String>) result.get("videoUrls");
                    exhibition.setText(result.getString("text"));
                    exhibition.setImageUrl(result.getString("imageUrl"));
                    exhibition.setAudioUrl(result.getString("audioUrl"));
                    exhibition.setVideoUrl(result.getString("videoUrl"));

                    List<Item> itemsList = new ArrayList<>();
                    String[] additionalImages = new String[items.size()];
                    JSONArray itemIds = (JSONArray) result.get("itemsIds");
                    if (itemIds.length() != 0 && !items.isEmpty()) {
                        int j = 0;
                        for (Item p : items) {
                            if (itemIds.toString().contains(p.getId())) {
                                itemsList.add(p);
                                additionalImages[j]= p.getImageUrl();
                                j++;
                            }
                        }
                    }
                    exhibition.setItems(itemsList);
                    exhibition.setAdditionalImagesUrl(additionalImages);

                    List<MuseumMap> museummapList = new ArrayList<>();
                    JSONArray museummapIds = (JSONArray) result.get("museummapsIds");
                    if (museummapIds.length() != 0 && !museumMaps.isEmpty()) {
                        for (MuseumMap m : museumMaps) {
                            if (museummapIds.toString().contains(m.getId())) {
                                museummapList.add(m);
                            }
                        }
                    }
                    exhibition.setMuseumMaps(museummapList);
                    exhibitions.add(exhibition);
                }
            }

        } catch (ExecutionException | JSONException | InterruptedException e) {
            e.printStackTrace();
        }
        return exhibitions;
    }
}
